(ql:quickload '(:cl-who :hunchentoot :css-lite))

(defpackage :zoop
  (:use :cl :cl-who :hunchentoot :css-lite))

(in-package :zoop)

(setf (html-mode) :HTML5)

(defvar *zoop-text*
  (with-html-output-to-string (s nil :prologue t)
    (:html
     (:head
      (:title "zoop")
      (:style :type "text/css"
	      (str (css-lite:css
		     (("body")
		      (:font-family "sans-serif"
		       :max-width "72ch"
		       :margin "auto"))))))
     (:body
      (:h1 "Hey~@[ ~A~], you've been 👉😎👉 Zooped!")))))

(hunchentoot:define-easy-handler (zoop :uri "/zoop") (name)
  (setf (hunchentoot:content-type*) "text/html")
  (format nil *zoop-text* name))

(hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port 4242))
